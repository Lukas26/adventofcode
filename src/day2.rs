pub use std::fs::File;
pub use std::io::BufRead;
pub use std::io::BufReader;
pub use std::vec::Vec;

#[derive(Debug)]
pub struct Computer {
    calcs: Vec<usize>,
}
impl Computer {
    pub fn new() -> Computer {
        Computer { calcs: vec![] }
    }
    #[allow(unused)]
    pub fn parse_input(self: &mut Self) {
        let file = File::open("res/input2.txt").expect("could not open file");
        let mut reader = BufReader::new(&file);

        let mut contents = Default::default();
        reader
            .read_line(&mut contents)
            .expect("could not read Input");
        self.calcs = contents
            .split(',')
            .map(|i| {
                i.parse::<usize>()
                    .expect(&format!("could not convert {} to usize", i))
            })
            .collect();
        self.calcs[1] = 12;
        self.calcs[2] = 2;
    }
    #[allow(unused)]
    pub fn runcalc(self: &mut Self) {
        let mut i: usize = 0;

        loop {
            let position = self.calcs[i + 3];
            match self.calcs[i] {
                1 => {
                    self.calcs[position] =
                        self.calcs[self.calcs[i + 2]] + self.calcs[self.calcs[i + 1]];
                }
                2 => {
                    self.calcs[position] =
                        self.calcs[self.calcs[i + 2]] * self.calcs[self.calcs[i + 1]];
                }
                99 => break,
                _ => {}
            }
            i += 4;
        }
    }
    #[allow(unused)]
    pub fn solve1(self: &mut Self) {
        self.parse_input();
        self.runcalc();
        println!("part 1: {}", self.calcs[0]);
    }
    #[allow(unused)]
    pub fn solve2(self: &mut Self) {
        for noun in 1..99 {
            for verb in 1..99 {
                self.parse_input();
                self.calcs[1] = noun;
                self.calcs[2] = verb;

                self.runcalc();
                if self.calcs[0] == 19690720 {
                    println!("part 2: {}", noun * 100 + verb)
                }
            }
        }
    }
}

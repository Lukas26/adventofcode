use crate::day2::{BufRead, BufReader, File};

pub fn solve_day5() {
    let mut c = Computer::new();
    c.parse_input();
    c.intcode(1);
}
pub struct Computer {
    data: Vec<isize>,
}
impl Computer {
    pub fn new() -> Computer {
        Computer { data: vec![] }
    }
    #[allow(unused)]
    pub fn parse_input(self: &mut Self) {
        let file = File::open("res/input5.txt").expect("could not open file");
        let mut reader = BufReader::new(&file);

        let mut contents = Default::default();
        reader
            .read_line(&mut contents)
            .expect("could not read Input");
        self.data = contents
            .split(',')
            .map(|i| {
                i.parse::<isize>()
                    .expect(&format!("could not convert {} to usize", i))
            })
            .collect();
        self.data[1] = 12;
        self.data[2] = 2;
    }
    #[allow(unused)]
    pub fn intcode(self: &mut Self, input: isize) {
        println!("{:?}", self.data);
        let mut i: usize = 0;
        let mut modes = vec![0, 0, 0, 0];
        let mut opcode = self.data[i] as i32;
        loop {
            if self.data[i] > 1000 {
                modes = number_to_vec(self.data[i] as i32);
                opcode = *modes.get(3).unwrap();
            } else {
                opcode = self.data[i] as i32;
            }
            let mut position = self.data[i + 3] as usize;
            println!("opcode:{}", opcode);
            match opcode {
                1 => {
                    self.data[position] =
                        self.data[self.data[i + 2] as usize] + self.data[self.data[i + 1] as usize];
                    i += 4;
                }
                2 => {
                    self.data[position] =
                        self.data[self.data[i + 2] as usize] * self.data[self.data[i + 1] as usize];
                    i += 4;
                }
                3 => {
                    position = self.data[i + 1] as usize;
                    self.data[position] = input;
                    i += 2;
                }
                4 => {
                    position = self.data[i + 1] as usize;
                    println!("{}", self.data[position]);
                    i += 2;
                }
                99 => break,
                _ => {
                    i += 6;
                }
            }
        }
    }
}

fn number_to_vec(n: i32) -> Vec<i32> {
    let mut digits = Vec::new();
    let mut n = n;
    while n > 9 {
        digits.push(n % 10);
        n = n / 10;
    }
    digits.push(n);
    digits.reverse();
    digits
}

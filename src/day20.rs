pub fn solve_day20() {
    parse_input();
}

fn parse_input() {
    let content_str: String = include_str!("../res/input20.txt").to_string();
    println!("{}", content_str);
}

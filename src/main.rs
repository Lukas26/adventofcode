#[allow(non_snake_case)]
mod day1;
mod day2;
mod day20;
mod day3;
mod day4;
mod day5;
mod day6;
mod day8;
use day1::Spacecraft;
use day2::Computer;
use day20::*;
use day3::*;
use day4::*;
use day5::*;
use day6::*;
use day8::*;

fn main() {
    day20();
}
#[allow(unused)]
fn day1() {
    let mut s = Spacecraft::new();

    s.calculate_fuel();
    println!("fuel required = {}", s.fuel);
    println!("fuel_fuel required = {}", s.fuel_for_fuel);

    println!("sum = {}", s.fuel + s.fuel_for_fuel);
}
#[allow(unused)]
fn day2() {
    let mut comp = Computer::new();
    comp.solve1();
    comp.solve2();
}
#[allow(unused)]
fn day3() {
    solve_day3();
}
#[allow(unused)]
fn day4() {
    solve_day4();
}
#[allow(unused)]
fn day5() {
    solve_day5();
}
#[allow(unused)]
fn day6() {
    solve_day6();
}
#[allow(unused)]
fn day8() {
    solve_day8_part1();
    solve_day8_part2();
}
#[allow(unused)]
fn day20() {
    solve_day20();
}

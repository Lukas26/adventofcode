use std::hash::Hash;

const PIXELS_WIDE: usize = 25;
const PIXELS_TALL: usize = 6;
const RADIX: u32 = 10;

pub fn solve_day8_part1() {
    let layers = parse_input();
    let mut min0_layer = layers[0].clone();
    for layer in &layers {
        if layer.count0 < min0_layer.count0 {
            min0_layer = layer.clone();
        }
    }
    println!("part 1:{}", min0_layer.count1 * min0_layer.count2);
}
pub fn solve_day8_part2() {
    let layers = parse_input();
    let display = decode(layers);
    let mut linebreak = 0 as usize;
    for i in display {
        if linebreak == PIXELS_WIDE {
            linebreak = 0;
            print!("\n");
        }
        print!("{}", i);
        linebreak += 1;
    }
}
#[derive(Debug, Clone)]
struct Layer {
    pixel_values: Vec<u8>,
    pub count0: u64,
    pub count1: u64,
    pub count2: u64,
}

impl Layer {
    fn new(pixels: Vec<u8>) -> Layer {
        let mut count0 = 0 as u64;
        let mut count1 = 0 as u64;
        let mut count2 = 0 as u64;
        //TODO search for 0 in pixel_values
        for i in pixels.clone() {
            match i {
                0 => count0 += 1,
                1 => count1 += 1,
                2 => count2 += 1,
                _ => (),
            }
        }
        Layer {
            pixel_values: pixels,
            count0,
            count1,
            count2,
        }
    }
}
fn decode(layers: Vec<Layer>) -> Vec<u8> {
    let layers_owend = layers.to_owned();

    let mut display_buffer: Vec<u8> = Vec::new();
    for i in 0..PIXELS_TALL * PIXELS_WIDE {
        display_buffer.push(0);
    }
    let mut pos = 0 as usize;
    let mut i = 0 as usize;
    let mut j = 0 as usize;
    let last = layers_owend.last().unwrap();

    while j > layers_owend.len() {
        let previous_layer = layers_owend[layers_owend.len() - j].clone();
        display_buffer = last.pixel_values.clone();
        while i < display_buffer.len() {
            if display_buffer[i] == 2 {
                display_buffer[i] = previous_layer.pixel_values[i];
            }
            i += 1;
        }
        j += 1;
    }

    display_buffer
}
fn parse_input() -> Vec<Layer> {
    let content: String = include_str!("../res/input8.txt").parse().unwrap();
    let layer_lenght = PIXELS_WIDE * PIXELS_TALL;
    let mut layers: Vec<Layer> = Vec::new();
    let mut layer_content = String::new();
    let mut i = 0;
    while i < content.len() {
        if (i + layer_lenght <= content.len()) {
            layer_content = content[i..i + layer_lenght].to_string();
            layers.push(Layer::new(
                layer_content
                    .chars()
                    .map(|s| s.to_digit(RADIX).expect("could not parse char to digit") as u8)
                    .collect(),
            ));
            i += layer_lenght;
        } else {
            break;
        }
    }
    layers
}

use std::cmp::Ordering;

pub fn solve_day4() {
    let count1 = (156218..652527).filter(|s| check1(*s)).count();
    println!("part1:{}", count1);
    let count2 = (156218..652527).filter(|s| check2(*s)).count();
    println!("part2:{}", count2);
}
fn check1(mut pw: u32) -> bool {
    let mut repeat = 1;
    let mut double = false;
    let mut previous = pw % 10;
    pw /= 10;

    while pw != 0 {
        let c = pw % 10;
        match c.cmp(&previous) {
            Ordering::Greater => return false,
            Ordering::Equal => {
                repeat += 1;
                if repeat == 2 {
                    double = true;
                }
            }
            Ordering::Less => {
                repeat = 1;
            }
        }
        previous = c;
        pw /= 10;
    }
    double
}
fn check2(mut pw: u32) -> bool {
    let mut repeat = 1;
    let mut double = false;
    let mut previous = pw % 10;
    pw /= 10;

    while pw != 0 {
        let c = pw % 10;
        match c.cmp(&previous) {
            Ordering::Greater => return false,
            Ordering::Equal => {
                repeat += 1;
            }
            Ordering::Less => {
                if repeat == 2 {
                    double = true;
                }
                repeat = 1;
            }
        }
        previous = c;
        pw /= 10;
    }
    double || repeat == 2
}
#[test]
fn test_check1() {
    assert!(check1(111111));
    assert!(!check1(223450));
    assert!(!check1(123789));
}
#[test]
fn test_check2() {
    assert!(check2(112233));
    assert!(!check2(123444));
    assert!(check2(111122))
}

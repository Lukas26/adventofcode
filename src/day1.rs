use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

#[derive(Debug)]
pub struct Spacecraft {
    pub fuel: i64,
    pub fuel_for_fuel: i64,
}
impl Spacecraft {
    #[allow(unused)]
    pub fn new() -> Spacecraft {
        let fuel = 0;
        let fuel_for_fuel = 0;
        Spacecraft {
            fuel,
            fuel_for_fuel: 0,
        }
    }
    #[allow(unused)]
    pub fn calculate_fuel(self: &mut Self) {
        let file = File::open("res/input1.txt").expect("could not open file");
        let reader = BufReader::new(&file);

        let mut res: i64 = 0;
        let mut res_fuel: i64 = 0;

        for line in reader.lines() {
            let l = line.unwrap();

            let l: i64 = l.parse::<i64>().expect("could not convert String to i64");
            let mut temp = (l / 3) - 2;
            res += temp;

            while temp / 3 - 2 > 0 {
                res_fuel += fuel(temp);
                temp = temp / 3 - 2;
            }
        }
        self.fuel_for_fuel = res_fuel;
        self.fuel = res;
    }
}

pub fn fuel(input: i64) -> i64 {
    let result = input / 3 - 2;

    if result < 0 {
        0
    } else {
        result
    }
}

use std::collections::HashSet;

pub fn solve_day3() {
    let input = std::fs::read_to_string("res/input3.txt").expect("could not read file");
    let mut lines = input.lines();

    let steps_1 = parse_line(lines.next().unwrap());
    let steps_2 = parse_line(lines.next().unwrap());

    let positions_1: HashSet<_> = steps_1.iter().cloned().collect();
    let positions_2: HashSet<_> = steps_2.iter().cloned().collect();

    let collisions = positions_1.intersection(&positions_2);
    let closest_collision = collisions.clone().map(manhattan).min();
    println!("part1: {}", closest_collision.unwrap());
    let fastest_collision = collisions
        .map(|pos| {
            steps_1.iter().position(|x| x == pos).unwrap()
                + steps_2.iter().position(|x| x == pos).unwrap()
                + 2
        })
        .min();
    println!("part2: {}", fastest_collision.unwrap());
}
fn parse_step(step: &str) -> impl Iterator<Item = (isize, isize)> {
    let mut chars = step.chars();

    let coord = match chars.next() {
        Some('R') => (1, 0),
        Some('L') => (-1, 0),
        Some('U') => (0, -1),
        Some('D') => (0, 1),
        _ => (0, 0),
    };
    let dist: usize = chars.collect::<String>().parse().unwrap();

    std::iter::repeat(coord).take(dist)
}
fn parse_line(line: &str) -> Vec<(isize, isize)> {
    line.split(",")
        .flat_map(parse_step)
        .scan((0, 0), |pos, step| {
            pos.0 += step.0;
            pos.1 += step.1;
            Some(pos.clone())
        })
        .collect()
}
fn manhattan(pos: &(isize, isize)) -> usize {
    pos.0.abs() as usize + pos.1.abs() as usize
}
